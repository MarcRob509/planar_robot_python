import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import numpy as np

class Disturbance(Node): 
    def __init__(self):
        super().__init__('disturbance')
        self.publisher = self.create_publisher(JointState, '/qdot_disturbance', 10)
        self.timer = self.create_timer(0.1, self.timer_callback)
        self.amplitude = 0.5
        self.frequency = 1

    def timer_callback(self):
        joint_state = JointState()
        joint_state.velocity = [self.amplitude * np.sin(self.frequency * self.timer.period) for _ in range(2)]
        self.publisher.publish(joint_state)

def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    rclpy.shutdown()

if __name__ == '__main__':
    main()

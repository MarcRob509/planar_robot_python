import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import numpy as np

class Simulator(Node):
    def __init__(self):
        super().__init__('simulator_node')
        self.publisher_ = self.create_publisher(JointState, 'joint_states_topic', 10)
        self.subs_desired_joint_velocity = self.create_subscription(
            JointState,
            'desired_joint_velocity_topic',
            self.desired_joint_velocity_callback,
            10)
        self.subs_disturbance = self.create_subscription(
            JointState,
            'disturbance_topic',
            self.disturbance_callback,
            10)
        self.timer = self.create_timer(0.1, self.timer_callback)
        self.q = np.array([0, 0])
        self.q_dot_d = np.zeros(2)
        self.disturbance_velocity = np.zeros(2)

    def desired_joint_velocity_callback(self, desired_joint_velocity):
        self.q_dot_d = np.array([desired_joint_velocity.velocity[0], desired_joint_velocity.velocity[1]])

    def disturbance_callback(self, disturbance_velocity):
        self.disturbance_velocity = np.array([disturbance_velocity.velocity[0], disturbance_velocity.velocity[1]])

    def timer_callback(self):
        self.q = self.q + (self.q_dot_d + self.disturbance_velocity) * 0.1
        joint_state = JointState()
        joint_state.header.stamp = self.get_clock().now().to_msg()
        joint_state.name = ['joint_1', 'joint_2']
        joint_state.position = [self.q[0], self.q[1]]
        self.publisher_.publish(joint_state)

def main(args=None):
    rclpy.init(args=args)
    simulator = Simulator()
    rclpy.spin(simulator)
    simulator.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()

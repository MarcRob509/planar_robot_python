import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
from custom_messages.msg import RefPosition, KinData
import numpy as np
import os

class HighLevelManager(Node):
    def __init__(self):
        super().__init__('high_level_manager')
        self.setup_subscriptions_and_publications()
        self.load_reference_positions()
        self.trajectory_initialized = False
        self.launch_flag = False
        self.last_ref_position = None

    def setup_subscriptions_and_publications(self):
        self.kin_data_sub = self.create_subscription(
            KinData,
            '/kin_data',
            self.kin_data_callback,
            10)

        self.launch_sub = self.create_subscription(
            Bool,
            '/launch',
            self.launch_callback,
            10)

        self.ref_position_pub = self.create_publisher(
            RefPosition,
            '/ref_position',
            10)

    def load_reference_positions(self):
        try:
            file_path = os.path.join(os.getcwd(), 'src/planar_robot_python/trajectories/test.csv')
            self.reference_positions = np.loadtxt(file_path, delimiter=',')
        except FileNotFoundError:
            self.get_logger().error("Could not find trajectory file: trajectories/test.csv")
            rclpy.shutdown()

    def kin_data_callback(self, msg):
        if not self.trajectory_initialized:
            self.publish_reference_position(self.reference_positions[0])
            self.trajectory_initialized = True

        if self.launch_flag and self.last_ref_position is not None:
            if msg.x == self.last_ref_position.x and msg.y == self.last_ref_position.y:
                self.publish_reference_position(self.last_ref_position)

    def launch_callback(self, msg):
        self.launch_flag = not self.launch_flag

    def publish_reference_position(self, ref_position):
        msg = RefPosition()
        msg.x = ref_position[0]
        msg.y = ref_position[1]
        self.ref_position_pub.publish(msg)
        self.last_ref_position = msg

def main(args=None):
    rclpy.init(args=args)
    node = HighLevelManager()
    rclpy.spin(node)
    rclpy.shutdown()

if __name__ == '__main__':
    main()

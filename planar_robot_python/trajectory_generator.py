import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from custom_messages.msg import RefPosition, CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np


class TrajectoryGeneratorNode(Node):
    def __init__(self):
        super().__init__('trajectory_generator')
        self.subscription = self.create_subscription(
            RefPosition,
            '/ref_position',
            self.ref_position_callback,
            10)
        self.publisher = self.create_publisher(
            CartesianState,
            '/desired_state',
            10)
        self.current_state = None

    def ref_position_callback(self, msg):
        if self.current_state is None:
            self.current_state = CartesianState()
            self.current_state.x = msg.x
            self.current_state.y = msg.y
            self.current_state.xdot = 0.0
            self.current_state.ydot = 0.0
            self.publisher.publish(self.current_state)
        else:
            new_state = CartesianState()
            new_state.x = msg.x
            new_state.y = msg.y
            new_state.xdot = (msg.x - self.current_state.x) / msg.deltat
            new_state.ydot = (msg.y - self.current_state.y) / msg.deltat
            self.current_state = new_state
            self.publisher.publish(self.current_state)

def main(args=None):
    rclpy.init(args=args)
    node = TrajectoryGeneratorNode()
    rclpy.spin(node)
    rclpy.shutdown()

if __name__ == '__main__':
    main()
